/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.message.G43823;

/**
 *
 * @author Etienne
 */
public class MessageCheck implements Message {

    private final String word;
    private final User user;
    
    public MessageCheck(User user,String word){
        this.word=word;
        this.user=user;
    }
    
    @Override
    public Type getType() {
        return Type.CHECK;
    }

    @Override
    public User getAuthor() {
        return user;
    }

    @Override
    public User getRecipient() {
        return User.ADMIN;
    }

    @Override
    public Object getContent() {
        return word;
    }
    
}
