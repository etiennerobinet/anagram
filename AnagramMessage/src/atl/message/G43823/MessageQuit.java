/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.message.G43823;

/**
 *
 * @author Etienne
 */
public class MessageQuit implements Message {
    User author;

    public MessageQuit(User author) {
        this.author = author;
    }

    @Override
    public Type getType() {
        return Type.QUIT;
    }

    @Override
    public User getAuthor() {
        return author;
    }

    @Override
    public User getRecipient() {
        return User.ADMIN;
    }

    @Override
    public Object getContent() {
        return null;
    }
}
