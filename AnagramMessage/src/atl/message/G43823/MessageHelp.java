/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.message.G43823;

/**
 *
 * @author Etienne
 */
public class MessageHelp implements Message {

    private StringBuilder help;
    private User recipient;
    
    public MessageHelp(User recipient){
        this.recipient=recipient;
        help= new StringBuilder();
        help.append("\nUsage :\n");
        help.append("\ttaper check <word> pour proposer votre réponse\n");
        help.append("\ttaper pass pour passer le mot courant\n");
        help.append("\ttaper quit pour arrêter le programme\n");
        help.append("\n");
    }
    
    @Override
    public Type getType() {
        return Type.HELP;
    }

    @Override
    public User getAuthor() {
        return User.ADMIN;
    }

    @Override
    public User getRecipient() {
        return recipient;
    }

    @Override
    public Object getContent() {
        return help;
    }

}
