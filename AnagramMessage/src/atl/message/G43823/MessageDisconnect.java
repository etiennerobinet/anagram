/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.message.G43823;

/**
 *
 * @author Etienne
 */
public class MessageDisconnect implements Message{

    
    private final User author;

    public MessageDisconnect(User author) {
        this.author = author;
    }
    
    @Override
    public Type getType() {
        return Type.DISCONNECT;
    }

    @Override
    public User getAuthor() {
        return author;
    }

    @Override
    public User getRecipient() {
        return User.ADMIN;
    }

    @Override
    public Object getContent() {
        return author.getName();
    }
    
}
