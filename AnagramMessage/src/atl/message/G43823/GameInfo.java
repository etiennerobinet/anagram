/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.message.G43823;

/**
 *
 * @author Etienne
 */
public class GameInfo {
    private final int total;
    private final int Wordsleft;
    private final int solved;
    private final int tries;

    public GameInfo(int total, int Wordsleft, int solved, int tries) {
        this.total = total;
        this.Wordsleft = Wordsleft;
        this.solved = solved;
        this.tries = tries;
    }

    

    public int getWordsleft() {
        return Wordsleft;
    }

    public int getTries() {
        return tries;
    }
    
}
