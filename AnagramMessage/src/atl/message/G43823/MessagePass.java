/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.message.G43823;

/**
 *
 * @author Etienne
 */
public class MessagePass implements Message {

    User author;
    String word;

    public MessagePass(User author) {
        this.author = author;
    }
    
    public void setWord(String passed){
        word=passed;
    }

    @Override
    public Type getType() {
        return Type.PASS;
    }

    @Override
    public User getAuthor() {
        return author;
    }

    @Override
    public User getRecipient() {
        return User.ADMIN;
    }

    @Override
    public Object getContent() {
        return word;
    }

}
