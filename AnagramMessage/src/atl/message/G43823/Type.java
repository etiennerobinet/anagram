package atl.message.G43823;

/**
 * The <code> Type </code> represents the type of a message send between a user
 * and the server.
 */
public enum Type {

    INFO,
    MEMBERS,
    DISCONNECT,
    CHECK,
    PASS,
    QUIT,
    PROFILE,
    NEWGAME,
    HELP;
}
