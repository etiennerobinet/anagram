package atl.server.G43823;

import atl.server.G43823.anagram.Controller.Serverlog;
import atl.server.G43823.anagram.Controller.ServerFacade;
import atl.server.G43823.anagram.model.Model;
import atl.message.G43823.*;
import atl.server.G43823.anagram.exception.ModelException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnagramServer extends AbstractServer {

    private final static int PORT = 12345;
    static final String ID_MAPINFO = "ID";

    private Games games;

    private int clientId;
    private final Members members;

    public AnagramServer() throws IOException {
        super(PORT);
        members = new Members();
        games = new Games();
        clientId = 0;
        this.listen();
    }

    public Games getGames() {
        return games;
    }

    private static InetAddress getLocalAddress() {
        try {
            Enumeration<NetworkInterface> b = NetworkInterface.getNetworkInterfaces();
            while (b.hasMoreElements()) {
                for (InterfaceAddress f : b.nextElement().getInterfaceAddresses()) {
                    if (f.getAddress().isSiteLocalAddress()) {
                        return f.getAddress();
                    }
                }
            }
        } catch (SocketException e) {
            Logger.getLogger(AnagramServer.class.getName()).log(Level.SEVERE, "NetworkInterface error", e);
        }
        return null;
    }

    public Members getMembers() {
        return members;
    }

    public String getIP() {
        if (getLocalAddress() == null) {
            return "Unknown";
        }
        return getLocalAddress().getHostAddress();
    }

    public int getNbConnected() {
        return members.size();
    }

    public void quit() throws IOException {
        this.stopListening();
        this.close();
    }

    final synchronized int getNextId() {
        clientId++;
        return clientId;
    }

    @Override
    protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
        Message message = (Message) msg;
        Type type = message.getType();
        User author = message.getAuthor();
        switch (type) {
            case PROFILE:
                int memberId = (int) client.getInfo(ID_MAPINFO);
                members.changeName(author.getName(), memberId);
                Message messageName = new MessageProfile(memberId, author.getName());
                sendToClient(messageName, memberId);
                sendToAllClients(new MessageMembers(members));
                break;
            case DISCONNECT:
                if (games.getGame(author) == null) {
                    sendToClient(message, author);
                } else {
                    games.quitGame(author);
                    sendToClient(message, author);
                }
            try {
                disconnect(client);
            } catch (InterruptedException | IOException ex) {
                Logger.getLogger(AnagramServer.class.getName()).log(Level.SEVERE, null, ex);
            }
                sendToAllClients(new MessageMembers(members));
                break;
            case QUIT:
                if (games.getGame(author) != null) {
                    games.quitGame(author);
                }
                break;
            case NEWGAME:
                Serverlog log = new Serverlog(author, this);
                ServerFacade game = new ServerFacade(new Model(), log);
                game.start();
                games.addGame(game);
                break;
            case CHECK:
                String toCheck = (String) message.getContent();
                ServerFacade toBeChecked = games.getGame(author);
                try {
                    if (toBeChecked.checkWord(toCheck)) {
                        if (toBeChecked.canCheck()) {
                            StringBuilder correct = new StringBuilder();
                            sendToClient(new MessageInfo(new StringBuilder("Bravo"), User.ADMIN), author);
                            toBeChecked.goNext();
                        }
                    } else {
                        sendToClient(new MessageInfo(new StringBuilder("Mauvaise réponse. Essaie encore \n"), User.ADMIN), author);
                        MessageInfo infos = new MessageInfo(
                                games.getGame(author).getCurrentInfos(), author);
                        sendToClient(infos, message.getAuthor());
                    }
                } catch (ModelException ex) {
                    System.out.println(ex);
                }
                sendToClient(message, author);
                break;
            case PASS:
                MessagePass passed = (MessagePass) message;
                ServerFacade gameToPass = games.getGame(author);
                try {
                    passed.setWord(gameToPass.pass());
                    gameToPass.goNext();
                } catch (ModelException ex) {
                    System.out.println(ex);
                }

        }
        setChanged();
        notifyObservers(message);
    }

    @Override
    protected void clientConnected(ConnectionToClient client) {
        super.clientConnected(client);
        int memberId = members.add(getNextId(), client.getName(), client.getInetAddress());
        client.setInfo(ID_MAPINFO, memberId);
        sendToAllClients(new MessageMembers(members));
        setChanged();
        notifyObservers();

    }

    @Override
    protected void clientDisconnected(ConnectionToClient client) {
        sendToAllClients(new MessageMembers(members));
    }

    public void sendToClient(Message message, User recipient) {
        sendToClient(message, recipient.getId());
    }

    public void sendToClient(Message message, int clientId) {
        for (Thread thread : getClientConnections()) {
            ConnectionToClient client = (ConnectionToClient) thread;
            if ((int) client.getInfo(ID_MAPINFO) == clientId) {
                try {
                    client.sendToClient(message);
                } catch (IOException ex) {
                    Logger.getLogger(AnagramServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void disconnect(ConnectionToClient client) throws InterruptedException, IOException {
        int id = (int) client.getInfo(ID_MAPINFO);
        members.remove(id);
        client.close();
    }

}
