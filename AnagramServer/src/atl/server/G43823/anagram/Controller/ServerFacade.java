package atl.server.G43823.anagram.Controller;

import atl.message.G43823.User;
import atl.server.G43823.anagram.model.Facade;
import atl.server.G43823.ConnectionToClient;
import atl.server.G43823.anagram.exception.ModelException;
import atl.server.G43823.anagram.model.Model;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Etienne
 */
public class ServerFacade extends Thread {

    private Model game;
    private Serverlog log;
    private String current;
    private boolean running;
    private StringBuilder infos;

    public ServerFacade(Model game, Serverlog log) {
        this.game = game;
        this.log = log;
        running = true;
        infos = new StringBuilder("");
    }

    public void gameStart() throws ModelException {
        try {
            game.initialize();
        } catch (ModelException e) {
            System.out.println(e);
        }
        game.start();
        log.displayHelp();
        log.display(getNextInfos());
        current = game.nextWord();
        log.display(getCurrentInfos());
        buildInfos();
    }

    public synchronized User getUser() {
        return log.getUser();
    }

    public synchronized String pass() throws ModelException {
        String passed = game.pass();
        StringBuilder info = new StringBuilder();
        info.append("You passed: ").append(passed).append("\n");
        log.display(info);
        buildInfos();
        return passed;
    }

    public synchronized boolean checkWord(String word) throws ModelException {
        if (game.propose(word)) {
            answer(word);
            buildInfos();
            return true;
        } else {
            return false;
        }
    }

    public synchronized boolean canCheck() {
        return game.canAskNextWord();
    }

    public synchronized void goNext() throws ModelException {
        if (game.canAskNextWord()) {
            log.display(getNextInfos());
            current = game.nextWord();
            buildInfos();
            log.display(getCurrentInfos());
        }
    }

    public void quitGame() {
        running = false;
    }

    public StringBuilder getNextInfos() throws ModelException {
        StringBuilder infos = new StringBuilder();
        infos.append("Il reste ").append(game.getNbRemainingWords()).
                append(" mot(s) à deviner sur les ").append(game.getNbWords()).
                append(" mots disponibles\n ");
        infos.append("Vous avez trouvé ").append(game.getNbSolvedWords()).
                append(" mot(s) et échoué sur ").append(game.getNbUnsolvedWords()).
                append(" mot(s)\n ");
        return infos;
    }

    public StringBuilder getCurrentInfos() throws ModelException {
        StringBuilder infos = new StringBuilder();
        infos.append("Quel est l'anagramme de ").append(current).append("\n");
        infos.append(game.getNbProposal()).append(" proposition(s) pour ce mot.\n");
        return infos;
    }

    @Override
    public void run() {
        this.setName("Game_" + log.getUser().getName());
        try {
            gameStart();
            while (running & !game.isOver()) {
                }
            } catch (ModelException ex) {
            Logger.getLogger(ServerFacade.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public boolean is(ServerFacade game){
        return game==this;
    }

    public void answer(String word) throws ModelException {
        StringBuilder right = new StringBuilder();
        right.append("You guessed:").append(word.toUpperCase()).append("\n");
        log.display(right);
    }

    private void buildInfos() throws ModelException {
        infos.setLength(0);
        infos.append(log.getUser().getName()).append("\t\t");
        infos.append(game.getNbSolvedWords()).append("\t").
                append(game.getNbUnsolvedWords()).append("\t");
        infos.append(game.getNbRemainingWords()).append("\t\t").
                append(game.getNbWords());
        infos.append("\n");
    }

    public StringBuilder getGameInfo() {
        return infos;
    }
}
