/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.server.G43823.anagram.Controller;

import atl.message.G43823.Message;
import atl.message.G43823.MessageHelp;
import atl.message.G43823.MessageInfo;
import atl.message.G43823.User;
import atl.server.G43823.AnagramServer;
import atl.server.G43823.anagram.model.Model;

/**
 *
 * @author Etienne
 */
public class Serverlog {
    private Message input;
    private final User client;
    private final AnagramServer server;

    public Serverlog(User client, AnagramServer server) {
        this.client = client;
        this.server = server;
    }

    void displayHelp() {
        MessageHelp help = new MessageHelp(client);
        server.sendToClient(help, client);        
    }
    
    public User getUser(){
        return client;
    }

    void display(StringBuilder infos) {
        server.sendToClient(new MessageInfo(infos,client), client);
    }
    
}
