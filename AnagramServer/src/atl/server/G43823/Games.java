/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.server.G43823;

import atl.message.G43823.User;
import atl.server.G43823.anagram.Controller.ServerFacade;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Etienne
 */
class Games {
    private List<ServerFacade> games;
    
    public Games(){
        games = new ArrayList<ServerFacade>();
    }
    
    public ServerFacade getGame(User user){
        ServerFacade searched=null;
        for(ServerFacade game : games){
            if(game.getUser().getId()==user.getId()){
                searched= game;
            }
        }
        return searched;
    }
    
    public void addGame(ServerFacade game){
        games.add(game);
    }

    public int nbOfGames() {
        return games.size();
    }
    
    public List<ServerFacade> getList(){
        return games;
    }

    void quitGame(User author) {
        ServerFacade gameToCancel =getGame(author);
        gameToCancel.quitGame();
        Iterator<ServerFacade> it = games.iterator();
        boolean find = false;
        ServerFacade current = null;
        while (it.hasNext() && !find) {
            current = it.next();
            find = current.is(gameToCancel);
        }
        if (find) {
            games.remove(current);
        }
        
    }
}
