package atl.server.G43823;

import atl.message.G43823.Message;
import atl.message.G43823.Type;
import atl.message.G43823.User;
import atl.server.G43823.anagram.Controller.ServerFacade;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Etienne
 */
public class ServerConsole implements Observer {

    public static void main(String[] args) {
        AnagramServer server = null;
        try {
            server = new AnagramServer();
            ServerConsole console = new ServerConsole(server);
            System.out.println("Adresse: " + server.getIP() + ":" + server.getPort());
        } catch (IOException ex) {
            Logger.getLogger(AnagramServer.class.getName()).log(Level.SEVERE, null, ex);
            try {
                server.quit();
            } catch (IOException ex1) {
                Logger.getLogger(ServerConsole.class.getName()).log(Level.SEVERE, null, ex1);
            }
            System.exit(0);
        }

    }

    private final AnagramServer model;

    public ServerConsole(AnagramServer model) {
        this.model = model;
        this.model.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg != null) {
            Message msg = (Message) arg;
            Type type = msg.getType();
            switch (type) {
                case PROFILE:
                    updateUsers();
                    break;
                case CHECK:
                    System.out.println(msg.getAuthor().getName()
                            + " tested :" + (String) msg.getContent().toString().toUpperCase());
                    updateGames();
                    break;
                case NEWGAME:
                    System.out.println("New game for: " + msg.getAuthor().getName());
                    break;
                case PASS:
                    System.out.println(msg.getAuthor().getName() + " passed on " + msg.getContent());
                    updateGames();
                    break;
                case DISCONNECT:
                    System.out.println("User disconnected: " + msg.getAuthor().getName());
                    updateUsers();
                    updateGames();
                    break;
                case QUIT:
                    System.out.println(msg.getAuthor().getName() + " cancelled his game");
                    updateGames();
                    break;
                default:
                    System.out.println("Invalid message type: " + type.toString());
                    break;
            }
        }
    }

    private void updateUsers() {
        System.out.println("");
        StringBuilder builder = new StringBuilder();
        builder.append("\n---- ---- Liste Users ---- ----\n");
        builder.append("Nombre d'utilisateurs connectes : ")
                .append(model.getNbConnected()).append("\n");
        builder.append("ID").append("\t");
        builder.append("IP").append("\t\t");
        builder.append("NAME").append("\n");
        for (User member : model.getMembers()) {
            builder.append(member.getId()).append("\t");
            builder.append(member.getAddress()).append("\t");
            builder.append(member.getName()).append("\n");
        }
        System.out.println(builder);
        System.out.println("");
    }

    private void updateGames() {
        System.out.println("");
        StringBuilder builder = new StringBuilder();
        builder.append("\n ---------------------Current games:");
        builder.append(model.getGames().nbOfGames());
        builder.append("--------------------\n");
        builder.append("Name").append("\t\t");
        builder.append("Solved").append("\t");
        builder.append("Passed").append("\t");
        builder.append("Remaining").append("\t");
        builder.append("Total").append("\t");
        builder.append("\n");
        for (ServerFacade game : model.getGames().getList()) {
            builder.append(game.getGameInfo());
        }
        System.out.println(builder);
        System.out.println("");
    }
}
