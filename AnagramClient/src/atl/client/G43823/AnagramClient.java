package atl.client.G43823;

import atl.message.G43823.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Etienne
 */
public class AnagramClient extends AbstractClient {

    private User mySelf;
    private Members members;

    public AnagramClient(String host, int port, String name) throws IOException {
        super(host, port);
        openConnection();
        sendToServer(new MessageProfile(0, name));
        members = new Members();
    }
    
    public Members getMembers(){
        return members;
    }

    @Override
    protected void handleMessageFromServer(Object msg) {
        try {
            Message message = (Message) msg;
            Type type = message.getType();
            switch (type) {
                case PROFILE:
                    setMySelf(message.getAuthor());
                    break;
                case MEMBERS:
                    Members members = (Members) message.getContent();
                    updateMembers(members);
                    break;
                case DISCONNECT:
                    disconnect();
                    break;
                case QUIT:
                    showMessage(message);
                    break;
                case HELP:
                    showMessage(message);
                case INFO:
                    showMessage(message);
                    break;
                case PASS:
                    showMessage(message);
                default:
                    //throw new IllegalArgumentException("Message type unknown " + type);
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            Logger.getLogger(AnagramClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        setChanged();
        notifyObservers(msg);
    }

    void disconnect() throws IOException {
        closeConnection();
        System.exit(0);
    }

    public User getMySelf() {
        return mySelf;
    }

    private void setMySelf(User user) {
        this.mySelf = user;
    }

    void checkWord(String word) throws IOException {
        MessageCheck msg = new MessageCheck(mySelf, word);
        sendToServer(msg);
    }

    private void showMessage(Message message) {
        notifyObservers(message);
    }

    public void newGame() throws IOException {
        sendToServer(new MessageNewGame(mySelf));
    }

    void pass() throws IOException {
        sendToServer(new MessagePass(mySelf));
    }

    public int getConnectedUsers() {
        return members.size();
    }

    private void updateMembers(Members members) {
        this.members.clear();
        for (User member : members) {
            this.members.add(member);
        }

    }
    
    


    

}
