/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atl.client.G43823;

import atl.message.G43823.*;
import atl.message.G43823.Type;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Etienne
 */
public class ClientConsole implements Observer {

    public static void main(String[] args) {
        AnagramClient client = null;
        try {
            Scanner in = new Scanner(System.in);
            String host = "localhost";
            int port = 12345;
            System.out.print("Name: ");
            String name = in.nextLine();
            client = new AnagramClient(host, port, name);
            ClientConsole console = new ClientConsole(client);
            while (true) {
                console.askForCmd();
            }

        } catch (IOException ex) {
            Logger.getLogger(ClientConsole.class.getName()).log(Level.SEVERE, "Main error", ex);
            try {
                client.disconnect();
            } catch (IOException quitEx) {
                Logger.getLogger(ClientConsole.class.getName()).log(Level.SEVERE, "Quit client error", quitEx);
            }
            System.exit(0);
        }
    }

    private final AnagramClient model;

    public ClientConsole(AnagramClient client) {
        this.model = client;
        this.model.addObserver(this);
    }

    private void askForCmd() {
        Scanner in = new Scanner(System.in);
        System.out.print(">");
        String cmd = in.nextLine();
        String tokens[] = cmd.split(" ");
        switch (tokens[0]) {
            case "help":
                displayUsage();
                break;
            case "quit":
            try {
                quitGame();
            } catch (IOException ex) {
                Logger.getLogger(ClientConsole.class.getName()).log(Level.SEVERE, null, ex);
            }
                break;
            case "exit":
            try {
                disconnect();
            } catch (IOException ex) {
                Logger.getLogger(ClientConsole.class.getName()).log(Level.SEVERE, null, ex);
            }
                break;
            case "members":
            displayUsers();
            break;
            case "check":
                if (tokens.length > 1) {
                    try {
                        model.checkWord(tokens[1]);
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                } else {
                    try {
                        System.out.println("Entrez le mot à check: ");
                        model.checkWord(in.next());
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }
                break;
            case "new":
                try {
                    model.newGame();
                } catch (IOException ex) {
                    System.out.println(ex);
                }
                break;
            case "pass":
                try {
                    model.pass();
                } catch (IOException ex) {
                    System.out.println(ex);
                }
                break;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg != null) {
            Message msg = (Message) arg;
            Type type = msg.getType();
            switch (type) {
                case PROFILE:
                    displayUsage();
                    break;
                case CHECK:
                    break;
                case DISCONNECT:
                    System.out.println("Disconnecting...");
                    break;
                case PASS:
                    System.out.println("You passed: " + msg.getContent());
                    break;
                case HELP:
                    StringBuilder help = (StringBuilder) msg.getContent();
                    System.out.println(help.toString());
                    break;
                case INFO:
                    StringBuilder infos = (StringBuilder) msg.getContent();
                    System.out.println(infos.toString());
                    break;
                default:

            }
        }
    }

    private void displayUsage() {
        StringBuilder use = new StringBuilder();
        use.append("Bienvenue sur le serveur Anagram!\n");
        use.append("Il y a actuellement ").append(model.getConnectedUsers()).
                append(" utilisateurs connecté(s)\n");
        use.append("\nCommandes: \n\t");
        use.append("help : Afficher l'aide\n\t");
        use.append("members : Afficher les utilisateur(s) connecté(s)\n\t");
        use.append("new : Démarrer une nouvelle partie\n\t");
        use.append("exit : Se déconnecter du serveur\n");
        System.out.println(use);
    }
    
    private void displayUsers(){
        System.out.println("");
        StringBuilder builder = new StringBuilder();
        builder.append("\n---- ---- Liste Users ---- ----\n");
        builder.append("Nombre d'utilisateurs connectes : ")
                .append(model.getConnectedUsers()).append("\n");
        builder.append("ID").append("\t");
        builder.append("IP").append("\t\t");
        builder.append("NAME").append("\n");
        for (User member : model.getMembers()) {
            builder.append(member.getId()).append("\t");
            builder.append(member.getAddress()).append("\t");
            builder.append(member.getName()).append("\n");
        }
        System.out.println(builder);
        System.out.println("");
    }

    private void disconnect() throws IOException {
        model.sendToServer(new MessageDisconnect(model.getMySelf()));
    }

    private void quitGame() throws IOException {
        model.sendToServer(new MessageQuit(model.getMySelf()));
    }
}
